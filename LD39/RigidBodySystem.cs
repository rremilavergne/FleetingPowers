﻿
using Nez;
using System.Collections.Generic;

namespace LD39
{
    public class RigidBodySystem : EntitySystem
    {
        public RigidBodySystem()
            : base(new Matcher().all(typeof(RigidBody)))
        { }

        protected override void process(List<Entity> entities)
        {
            List<(RigidBody r, CollisionResult c)> moves = new List<(RigidBody r, CollisionResult c)>();
            foreach (var entity in entities)
            {
                CollisionResult collisionResult;

                var rigidBody = entity.getComponent<RigidBody>();
                var collider = entity.getComponent<Collider>();
                var neighbors = Physics.boxcastBroadphaseExcludingSelf(collider, collider.collidesWithLayers);
                foreach (var neighbor in neighbors)
                {
                    if (collider.collidesWith(neighbor, out collisionResult))
                    {
                        moves.Add((rigidBody, collisionResult));
                    }
                }
            }

            foreach ((var r, var c)  in moves)
            {
                r.entity.position -= c.minimumTranslationVector;
                r.Collide(c);
            }
        }
    }
}
