﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Nez;
using Nez.Tiled;
using Nez.Sprites;
using Nez.Particles;
using Nez.UI;
using Nez.Audio;


using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;

namespace LD39
{
    public class ParticleFinisher : Component, IUpdatable
    {

        public void update()
        {
            foreach (var emitter in entity.getComponents<ParticleEmitter>())
            {
                if (!emitter.isStopped)
                    return;
            }
            entity.destroy();
        }
    }

    public class Attack : Entity
    {
        public virtual Texture2D Icon
        {
            get
            {
                return null;
            }
        }
        public virtual Color trailcolor => Color.White;

    }
    
    public class Timeout : Component, IUpdatable
    {
        public float timeout;

        public void update()
        {
            timeout -= Time.deltaTime;
            if (timeout < 0)
                entity.destroy();
        }
    }

    public class FireballAttack : Attack
    {
        public override Texture2D Icon => Core.content.Load<Texture2D>("Icons/fire");
        public override Color trailcolor => Color.Orange;

        Vector2 velocity;
        float timeout = 1f;

        SoundEffect sound;

        public override void onAddedToScene()
        {
            base.onAddedToScene();
            velocity = scene.camera.mouseToWorldPoint() - position;
            velocity = velocity.normalized() * 600f;
            velocity += new Vector2(Nez.Random.minusOneToOne() * 200, Nez.Random.minusOneToOne() * 200);

            addComponent(new Sprite(
                scene.content.Load<Texture2D>("fireball")
                ));
            addComponent(new CircleCollider(20f) { physicsLayer = 1 << 1, collidesWithLayers = (1 << 0) + (1 << 2) });
            addComponent(new RigidBody()).Collided +=
                (result) =>
                {
                    if (result.collider.entity is Enemy)
                        (result.collider.entity as Enemy).Damage();
                    end();
                };

            sound = scene.content.Load<SoundEffect>("sounds/fireball-explode");

            var config = new ParticleEmitterConfig();
            config.subtexture = new Nez.Textures.Subtexture(Core.content.Load<Texture2D>("particleflame"));
            config.emissionRate = 30.0f;
            config.sourcePositionVariance = new Vector2(8f, 8f);
            config.simulateInWorldSpace = true;
            config.particleLifespan = 0.25f;
            config.duration = 50;
            config.startParticleSize = 16f;
            config.finishParticleSize = 8f;
            config.rotationStart = 2f;
            config.startColor = Color.White;
            config.finishColor = Color.Transparent;
            config.maxParticles = 50;
            addComponent(new ParticleEmitter(config))
                .play();
        }

        public void end()
        {

            sound.Play();
            Game1.Shake(6);
            var config = new ParticleEmitterConfig();
            config.subtexture = new Nez.Textures.Subtexture(Core.content.Load<Texture2D>("particleflame"));
            config.emissionRate = 800.0f;
            config.duration = 0.035f;
            config.particleLifespan = 0.5f;
            config.particleLifespanVariance = 0.2f;
            config.speed = 100f;
            config.speedVariance = 70f;
            config.angleVariance = 360f;
            config.startParticleSize = 16f;
            config.finishParticleSize = 8f;
            config.startColor = Color.White;
            config.finishColor = Color.Transparent;
            config.maxParticles = 20000;

            var emitter = scene.createEntity("")
                .addComponent(new ParticleEmitter(config))
                .entity.setPosition(position)
                ;

            emitter.addComponent(new Timeout { timeout = config.duration + config.particleLifespan + config.particleLifespanVariance });
            emitter.tweenPositionTo(emitter.position, config.duration + config.particleLifespan + config.particleLifespanVariance)
                .start()
                ;
            destroy();
        }

        public override void update()
        {
            base.update();
            position += velocity * Time.deltaTime;
            rotation = Nez.Random.minusOneToOne() * 3.14f;
            timeout -= Time.deltaTime;
            if (timeout < 0)
                end();
        }
    }

    public class FireAttack : Attack
    {
        public override Texture2D Icon => Core.content.Load<Texture2D>("Icons/fire");
        public override Color trailcolor => Color.Orange;

        public override void onAddedToScene()
        {
            base.onAddedToScene();
            Game1.Shake(8);
            for (int i = 0; i < 10; ++i)
            {
                scene.content.Load<SoundEffect>("sounds/fireball").Play();
                scene.addEntity(new FireballAttack()).setPosition(position);
            }
        }

    }

    public class LightningAttack : Attack
    {
        public override Texture2D Icon => Core.content.Load<Texture2D>("Icons/lightning");
        public override Color trailcolor => Color.LightBlue;

        class Point
        {
            public List<Point> nextPoints = new List<Point>();
            public Vector2 coord;
        }

        class PointRenderer : RenderableComponent
        {
            public override float width => 600;
            public override float height => 600;

            void renderPoint(Graphics g, Point p)
            {
                foreach (var np in p.nextPoints)
                {
                    g.batcher.drawLine(p.coord, np.coord, Color.White, 4);
                    g.batcher.drawLine(p.coord, np.coord, Color.LightBlue, 2);
                    renderPoint(g, np);
                }
            }

            public override void render(Graphics graphics, Camera camera)
            {
                renderPoint(graphics, (entity as LightningAttack).rootPoint);
            }
        }

        Point rootPoint;
        Vector2 dir;
        float timeout = 0.5f;


        public override void onAddedToScene()
        {
            dir = scene.camera.mouseToWorldPoint() - position;
            scene.content.Load<SoundEffect>("sounds/lightning").Play();
            Game1.Shake(10);
            rootPoint = (new Point() { coord = position });

            addComponent(new PointRenderer());
        }

        void collisionPoint(Point p)
        {
            var nnexpoints = new List<Point>();
            foreach (var np in p.nextPoints)
            {
                var cast = Physics.linecast(p.coord, np.coord);
                if (cast.collider != null)
                {
                    if (cast.collider.entity is Enemy)
                        (cast.collider.entity as Enemy).Damage();
                }
                else
                {
                    collisionPoint(np);
                    nnexpoints.Add(np);
                }
                p.nextPoints = nnexpoints;
            }
        }

        public override void update()
        {
            rootPoint.nextPoints.Clear();
            var point = rootPoint;
            for (int i = 0; i < 6; i++)
            {
                var ndiff = Vector2.Transform(dir.normalized(), Matrix.CreateRotationZ(Nez.Random.minusOneToOne() * 3.14f / 4)) * 40f;
                var npoint = (new Point() { coord = point.coord + ndiff });
                point.nextPoints.Add(npoint);
                point = npoint;
            }
            timeout -= Time.deltaTime;
            collisionPoint(rootPoint);
            if (timeout < 0)
                destroy();
            base.update();
        }
    }

    public class BeamAttack : Attack
    {
        Vector2 start, end;

        class BeamRenderer : RenderableComponent
        {
            public override float width => 600;
            public override float height => 600;

            public override void render(Graphics graphics, Camera camera)
            {
                var b = entity as BeamAttack;
                graphics.batcher.drawLine(b.start, b.end, Color.LightYellow, 14);
                graphics.batcher.drawLine(b.start, b.end, Color.White, 10);
            }
        }

        public override Texture2D Icon => Core.content.Load<Texture2D>("Icons/beam");
        public override Color trailcolor => Color.LightYellow;

        public override void onAddedToScene()
        {
            start = position;
            Game1.Shake(10);
            scene.content.Load<SoundEffect>("sounds/beam").Play();
            addComponent(new BeamRenderer());
        }

        float timeout = 0.5f;

        public override void update()
        {
            base.update();
            start = scene.findEntity("player").position;
            var mouse = scene.camera.mouseToWorldPoint();
            var cend = Physics.linecast(start, start + (mouse - start) * 10000);
            if (cend.collider != null && cend.collider.entity is Enemy)
                (cend.collider.entity as Enemy).Damage();
            timeout -= Time.deltaTime;
            if (timeout < 0)
                destroy();
            end = cend.point;
        }

    }

    public class PlaceRockAttack : Attack
    {

        public override Texture2D Icon => Core.content.Load<Texture2D>("Icons/rock");
        float timeout = 10;
        float placeTimeout = 0.5f;

        public override void onAddedToScene()
        {
            base.onAddedToScene();
            Game1.Shake(2);
            scene.content.Load<SoundEffect>("sounds/rock").Play();
            position = scene.camera.mouseToWorldPoint();
            addComponent(new Sprite(
                scene.content.Load<Texture2D>("rock")
                )).layerDepth = Mathf.map01(-position.Y, -10000, 10000);

            this.setRotation(Nez.Random.nextAngle());
            addComponent(new BoxCollider(48, 48));

            var config = new ParticleEmitterConfig();
            config.subtexture = new Nez.Textures.Subtexture(Core.content.Load<Texture2D>("particlewind"));
            config.emissionRate = 80.0f;
            config.sourcePositionVariance = new Vector2(20f, 20f);
            config.particleLifespan = 0.6f;
            config.duration = 0.1f;
            config.angleVariance = 360;
            config.speed = 30;
            config.rotationStartVariance = 80f;
            config.startParticleSize = 8f;
            config.finishParticleSize = 8f;
            config.startColor = Color.SandyBrown;
            config.finishColor = Color.Transparent;
            config.maxParticles = 10000;
            addComponent(new ParticleEmitter(config))
                .play();
        }

        public override void update()
        {
            timeout -= Time.deltaTime;
            if (timeout < 0)
                destroy();
            base.update();
        }
    }

    public class RockAttack : Attack
    {
        public override Texture2D Icon => Core.content.Load<Texture2D>("Icons/rock");
        public override Color trailcolor => Color.Brown;
        float timeout = 0.25f;
        float placeRate = 0.03f;
        float placeTime = 0f;

        public override void update()
        {

            timeout -= Time.deltaTime;
            if (timeout < 0)
                destroy();
            placeTime -= Time.deltaTime;
            while (placeTime < 0f)
            {
                scene.addEntity(new PlaceRockAttack());
                placeTime += placeRate;
            }
        }
    }

    public class WindAttack : Attack
    {
        public override Texture2D Icon => Core.content.Load<Texture2D>("Icons/wind");
        public override Color trailcolor => Color.White;

        Vector2 velocity;

        float timeout = 2.5f;
        public override void onAddedToScene()
        {
            base.onAddedToScene();
            Game1.Shake(10);
            scene.content.Load<SoundEffect>("sounds/wind").Play();
            velocity = scene.camera.mouseToWorldPoint() - position;
            velocity = velocity.normalized() * 600f;

            addComponent(new BoxCollider(300, 100) { shouldColliderScaleAndRotateWithTransform = true,
                localOffset = new Vector2(150, 0),
                physicsLayer = 0
            });

            var mouse = scene.camera.mouseToWorldPoint();
            var angle = position.angleBetween(position + new Vector2(1f, 0f), mouse);
            if (position.Y > mouse.Y)
                this.setRotationDegrees(-angle);
            else
                this.setRotationDegrees(angle);

            var config = new ParticleEmitterConfig();
            config.subtexture = new Nez.Textures.Subtexture(Core.content.Load<Texture2D>("particlewind"));
            config.emissionRate = 400.0f;
            config.sourcePositionVariance = new Vector2(70f, 70f);
            config.particleLifespan = 0.5f;
            config.duration = 2.0f;
            config.angle = rotationDegrees;
            config.speed = 800f;
            config.startParticleSize = 8f;
            config.finishParticleSize = 8f;
            config.rotationStart = -2000f;
            config.rotationEnd = -200f;
            config.startColor = Color.White;
            config.finishColor = Color.Transparent;
            config.maxParticles = 10000;
            addComponent(new ParticleEmitter(config)
            {
                simulateInWorldSpace = false
            })
            .play();
        }

        public override void update()
        {
            base.update();
            foreach (var coll in Physics.boxcastBroadphaseExcludingSelf(getComponent<Collider>()))
            {
                CollisionResult res;
                if (getComponent<Collider>().collidesWith(coll, out res) &&
                    coll.entity is Enemy)
                    coll.entity.position += velocity * Time.deltaTime;

            }
            timeout -= Time.deltaTime;
            if (timeout < 0)
                destroy();

        }
    }

    public class BlackHoleAttack : Attack
    {
        
        public override Texture2D Icon => Core.content.Load<Texture2D>("Icons/black-hole");
        public override Color trailcolor => Color.Purple;

        float timeout = 2.0f;
        public override void onAddedToScene()
        {
            scene.content.Load<SoundEffect>("sounds/blackhole").Play();
            Game1.Shake(8);
            base.onAddedToScene();
            position = scene.camera.mouseToWorldPoint();
            addComponent(new Sprite(
                scene.content.Load<Texture2D>("vortex")
                ))
                .setColor(Color.White * 0.5f)
                .layerDepth = Mathf.map01(-position.Y, -10000, 10000);

            scale *= 4;
            this.tweenLocalScaleTo(0, timeout).start();
            addComponent(new CircleCollider(32f) { physicsLayer = 0 });

            var config = new ParticleEmitterConfig();
            config.subtexture = new Nez.Textures.Subtexture(Core.content.Load<Texture2D>("particlewind"));
            config.emissionRate = 10.0f;
            config.sourcePositionVariance = new Vector2(20f, 20f);
            config.particleLifespan = 0.6f;
            config.duration = 2.0f;
            config.angleVariance = 360;
            config.speed = 30;
            config.rotationStartVariance = 80f;
            config.startParticleSize = 8f;
            config.finishParticleSize = 8f;
            config.startColor = Color.Purple;
            config.finishColor = Color.Transparent;
            config.maxParticles = 10000;
            addComponent(new ParticleEmitter(config))
                .play();
        }

        public override void update()
        {
            base.update();
            foreach (var coll in Physics.boxcastBroadphaseExcludingSelf(getComponent<Collider>()))
            {
                CollisionResult res;
                if (getComponent<Collider>().collidesWith(coll, out res) &&
                    coll.entity is Enemy)
                    coll.entity.position += (position - coll.entity.position).normalized() * 200 * Time.deltaTime;

            }
            rotation -= 3.14f * 2 * Time.deltaTime;
            timeout -= Time.deltaTime;
            if (timeout < 0)
                destroy();

        }

    }

    public class WaterAttack : Attack
    {
        public override Texture2D Icon => Core.content.Load<Texture2D>("Icons/Wave");
        public override Color trailcolor => Color.Blue;

        Vector2 velocity;
        float timeout = 4f;

        public override void onAddedToScene()
        {
            base.onAddedToScene();
            Game1.Shake(10);
            scene.content.Load<SoundEffect>("sounds/wave").Play();
            velocity = scene.camera.mouseToWorldPoint() - position;
            velocity = velocity.normalized() * 600f;

            addComponent(new BoxCollider(20f, 100f) { physicsLayer = 0 });
            var mouse = scene.camera.mouseToWorldPoint();
            var angle = position.angleBetween(position + new Vector2(1f, 0f), mouse);
            if (position.Y > mouse.Y)
                this.setRotationDegrees(-angle);
            else
                this.setRotationDegrees(angle);


            for (int i = 0; i < 10; i++)
            {
                var nentity = scene.createEntity("this");
                nentity.setParent(this);
                nentity.setLocalPosition((new Vector2(0, -50f + i * 10f)));


                var config = new ParticleEmitterConfig();
                config.subtexture = new Nez.Textures.Subtexture(Core.content.Load<Texture2D>("particlewater"));
                config.emissionRate = 400.0f;
                config.sourcePositionVariance = new Vector2(10f, 10f);
                config.particleLifespan = 0.15f;
                config.duration = 50;
                config.startParticleSize = 16f;
                config.finishParticleSize = 16f;
                config.rotationStartVariance = (180f);
                config.blendFuncSource = Blend.One;
                config.blendFuncDestination = Blend.InverseSourceAlpha;
                config.startColor = Color.White;
                config.finishColor = Color.Transparent;
                config.maxParticles = 5000;
                nentity.addComponent(new ParticleEmitter(config));

                config = new ParticleEmitterConfig();
                config.subtexture = new Nez.Textures.Subtexture(Core.content.Load<Texture2D>("particlewind"));
                config.emissionRate = 40.0f;
                config.sourcePositionVariance = new Vector2(10f, 10f);
                config.particleLifespan = 0.5f;
                config.duration = 50;
                config.startParticleSize = 8f;
                config.finishParticleSize = 8f;
                config.rotationStartVariance = (180f);
                config.blendFuncSource = Blend.One;
                config.blendFuncDestination = Blend.InverseSourceAlpha;
                config.startColor = Color.White;
                config.finishColor = Color.White;
                config.maxParticles = 5000;
                config.simulateInWorldSpace = false;
                nentity.addComponent(new ParticleEmitter(config));
            }
        }

        public void end()
        {
            for (int i = 0; i < transform.childCount; ++i)
            {
                var c = transform.getChild(i);
                c.entity.enabled = false;
            }

            destroy();
        }

        public override void update()
        {
            base.update();
            position += velocity * Time.deltaTime;
            timeout -= Time.deltaTime;

            foreach (var coll in Physics.boxcastBroadphaseExcludingSelf(getComponent<Collider>()))
            {
                CollisionResult res;
                if (getComponent<Collider>().collidesWith(coll, out res) &&
                    coll.entity is Enemy)
                    coll.entity.position += velocity * 1.5f * Time.deltaTime;

            }
            if (timeout < 0)
                end();
        }
    }

    public class BladeAttack : Attack
    {
        public override Color trailcolor => Color.Gray;

        class SBlade : Entity
        {
            public override void onAddedToScene()
            {
                addComponent(new Sprite(
                    scene.content.Load<Texture2D>("sword")
                    )).setColor(Color.White * 0.75f);
                addComponent(new BoxCollider(3, 16) { shouldColliderScaleAndRotateWithTransform = true, collidesWithLayers = 1 << 2 });
                addComponent(new RigidBody()).Collided +=
                    (result) => {
                        if (result.collider.entity is Enemy e) e.Damage();
                        destroy();
                        scene.content.Load<SoundEffect>("sounds/hit").Play();
                        Game1.Shake(2);
                    };


            }

            public override void update()
            {
                base.update();
            }
        }
        public override Texture2D Icon => Core.content.Load<Texture2D>("Icons/blade");

        float timeout = 5.0f;

        float angle = 0f;

        public override void onAddedToScene()
        {
            scene.content.Load<SoundEffect>("sounds/summon").Play();
            base.onAddedToScene();
            this.setParent( scene.findEntity("player") );
            this.setLocalPosition(Vector2.Zero);
            this.setScale(2);
            this.setRotation(0);

            for (int i = 0; i < 10; i++)
            {
                var nblade = new SBlade();

                scene.addEntity(nblade);
                nblade.setParent(this);
                nblade.setLocalPosition(Vector2.Transform(Vector2.UnitX, Matrix.CreateRotationZ(2f * 3.14f * i / 10)) * 50);
                nblade.setRotation(2f * 3.14f * i / 10 + 1.57f);
            }
        }

        public override void update()
        {
            this.setRotation(angle);
            angle += Time.deltaTime * 2 * 3.14f;
            timeout -= Time.deltaTime;
            if (timeout < 0)
                destroy();
            base.update();
        }
    }

    public class BasicAttack : Entity
    {

        Vector2 velocity;
        float timeout = 0.2f;

        public override void onAddedToScene()
        {
            base.onAddedToScene();
            velocity = scene.camera.mouseToWorldPoint() - position;
            velocity = velocity.normalized() * 600f;
            scene.content.Load<SoundEffect>("sounds/hit").Play();
            Game1.Shake(2);

            addComponent(new Sprite(
                scene.content.Load<Texture2D>("sword")
                ));
            addComponent(new CircleCollider(8f) { physicsLayer = 1 << 1, collidesWithLayers = (1 << 0) + (1 << 2) });
            addComponent(new RigidBody()).Collided +=
                (result) =>
                {
                    if (result.collider.entity is Enemy)
                        (result.collider.entity as Enemy).Damage();
                    end();
                };
        }

        public void end()
        {
            scene.content.Load<SoundEffect>("sounds/hit").Play();
            destroy();
        }

        public override void update()
        {
            base.update();
            position += velocity * Time.deltaTime;
            rotation = Nez.Random.nextAngle();
            timeout -= Time.deltaTime;
            if (timeout < 0)
                end();
        }

    }

    public class AttackManager
    {
        public LinkedList<Attack> queue = new LinkedList<Attack>();
        public List<Attack> attacks = new List<Attack>();
        public List<int> left = new List<int>();
        int _currentAttack;

        public static Attack CurrentAttack
        {
            get { return Instance.queue?.First?.Value; }
        }

        public static void Consume()
        {
            Instance.queue.RemoveFirst();
        }

        static AttackManager _instance = null;
        public static AttackManager Instance
        {
            get {
                if (_instance == null)
                    _instance = new AttackManager();
                return _instance;
            }
        }

        public static Attack GetRandomAttack()
        {
            return Instance.attacks.randomItem();
        }

        AttackManager()
        {
            attacks.Add(new FireAttack());
            attacks.Add(new LightningAttack());
            attacks.Add(new BeamAttack());
            attacks.Add(new RockAttack());
            attacks.Add(new WindAttack());
            attacks.Add(new BlackHoleAttack());
            attacks.Add(new WaterAttack());
            attacks.Add(new BladeAttack());
        }
        public void update()
        {
            if (Input.isKeyReleased(Keys.Q))
                _currentAttack -= 1;
            if (Input.isKeyReleased(Keys.E))
                _currentAttack += 1;
            _currentAttack = (((_currentAttack % attacks.Count) + attacks.Count) % attacks.Count);

        }

        
    }

    public class Player : Entity
    {
        public float speed = 400f;
        public float acceleration = 0.1f;


        Vector2 velocity;
        bool dead = false;

        public override void onAddedToScene()
        {
            base.onAddedToScene();
            name = "player";
            this.addComponent(new Sprite(scene.content.Load<Texture2D>("char")))
                .addComponent(new CircleCollider(8f) { physicsLayer = 1 << 1, collidesWithLayers = 1 + 4 })
                .addComponent(new RigidBody())
                .entity.setScale(0.6f).setPosition(200, 200);
            this.getComponent<RigidBody>().Collided += OnCollide;
        }

        void OnCollide(CollisionResult result)
        {
            if (result.collider.entity is Enemy && !dead)
            {
                dead = true;
                var transition = new FadeTransition(() => Game1.deathScene())
                {
                    fadeOutDuration = 1.0f,
                    fadeInDuration = 0.25f,
                };
                Core.startSceneTransition(transition);
            }

        }

        void movementHandling()
        {
            var deltaVelocity = new Vector2(0f, 0f);
            if (Input.isKeyDown(Keys.W))
                deltaVelocity += new Vector2(0f, -1f);
            if (Input.isKeyDown(Keys.S))
                deltaVelocity += new Vector2(0f, 1f);
            if (Input.isKeyDown(Keys.A))
                deltaVelocity += new Vector2(-1f, 0f);
            if (Input.isKeyDown(Keys.D))
                deltaVelocity += new Vector2(1f, 0f);
            Vector2Ext.normalize(ref deltaVelocity);

            var tacceleration = speed * Time.deltaTime / acceleration;
            deltaVelocity *= tacceleration * 2;
            velocity += deltaVelocity;
            if (velocity.LengthSquared() > speed * speed)
                velocity = velocity / velocity.Length() * speed;

            var decel = Vector2Ext.normalize(velocity) * tacceleration;
            if (velocity.LengthSquared() < decel.LengthSquared())
                velocity = Vector2.Zero;
            else
                velocity -= decel;

            var mouse = scene.camera.mouseToWorldPoint();
            var angle = position.angleBetween(position + new Vector2(1f, 0f), mouse);
            if (position.Y > mouse.Y)
                this.setRotationDegrees(-angle);
            else
                this.setRotationDegrees(angle);
        }

        public float lastClickTime = 0f;

        void fireHandling()
        {
            if (Input.leftMouseButtonPressed && Time.time - lastClickTime > 0.5f)
            {
                lastClickTime = Time.time;
                var atk = AttackManager.CurrentAttack;
                if (atk != null)
                {
                    scene.addEntity(AttackManager.CurrentAttack.clone()).setPosition(position);
                    AttackManager.Consume();
                }
                else
                {
                    scene.addEntity(new BasicAttack()).setPosition(position);
                }
            }
        }

        public override void update()
        {
            base.update();
            if (dead)
                return;
            movementHandling();
            fireHandling();
            position += velocity * Time.deltaTime;
            scene.camera.position = position;
        }

    }

    public class HUD : Entity
    {
        Table hudTable = new Table();
        Label scoreLabel = new Label("0");
        ProgressBar bar;
        UICanvas canvas;

        List<Attack> attacks = AttackManager.Instance.attacks;

        public override void onAddedToScene()
        {
            canvas = addComponent(new UICanvas());
            canvas.renderLayer = 100;
            canvas.stage.addElement(hudTable);
            hudTable.setFillParent(true);
            hudTable.top().left();

        }

        public void rebuild()
        {
            hudTable.clear();
            hudTable.add(scoreLabel).pad(2);
            hudTable.row();

            foreach (var attack in AttackManager.Instance.queue)
            {
                var image = new Image(attack.Icon);
                var button = new Button(Skin.createDefaultSkin());
                button.add(image).pad(2);
                hudTable.add(button)
                    .setMaxWidth(36)
                    .top().left()
                    .setPadBottom(2)
                    ;
                if (attack == AttackManager.CurrentAttack)
                    button.getStyle().up = button.getStyle().down;

                hudTable.row();
            }

        }

        public override void update()
        {
            base.update();
            rebuild();
            scoreLabel.setText($"Score: {Game1.score}");
        }


    }

    public class Game1 : Core
    {
        public Game1()
            : base(windowTitle: "LD39", isFullScreen: false)
        { }

        public static void Shake(float strength = 15)
        {
            Core.scene.findEntity("shake").getComponent<CameraShake>().shake(strength);
        }

        public static Scene deathScene()
        {
            var nscene = Scene.createWithDefaultRenderer(Color.Black);
            var stage = nscene.createEntity("menu").addComponent(new UICanvas()).stage;
            var table = stage.addElement(new Table())
                .setFillParent(true);
            table.setTransform(true);

            var label = new Label("You died");
            label.setFontScale(5);
            table.add(label);
            table.row();


            var startbutton = new TextButton("Restart", Skin.createDefaultSkin());
            startbutton.onClicked += 
                (o) => { Core.startSceneTransition(new FadeTransition(() => gameScene()) { fadeInDuration = 0.5f, fadeOutDuration = 0.5f }); };
            table.add(startbutton)
                .pad(10);
            startbutton.setTransform(true);
            startbutton.setScale(2);

            return nscene;
        }

        static Scene startMenuScene()
        {
            var nscene = Scene.createWithDefaultRenderer(Color.Black);
            var stage = nscene.createEntity("menu").addComponent(new UICanvas()).stage;
            stage.addElement(new Image(Core.content.Load<Texture2D>("start")));
            var table = stage.addElement(new Table())
                .setFillParent(true);
            table.setTransform(true);
            
            var startbutton = new Button(Skin.createDefaultSkin());
            var label = new Label("Start");
            label.setFontScale(2);
            startbutton.add(label);
            startbutton.onClicked += 
                (o) => { Core.startSceneTransition(new FadeTransition(() => gameScene()) { fadeInDuration = 0.5f, fadeOutDuration = 0.5f }); };
            table.add(startbutton)
                .pad(10).setMinHeight(40).setMinWidth(60);
            startbutton.setTransform(true);
            startbutton.setScale(1);


            return nscene;
        }

        public static int score = 0;
        static Scene gameScene()
        {
            score = 0;
            AttackManager.Instance.queue.Clear();
            foreach (var atk in AttackManager.Instance.attacks)
                AttackManager.Instance.queue.AddLast(atk.clone() as Attack);
            var nscene = new Scene();
            nscene.addRenderer(new RenderLayerExcludeRenderer(0, 100));
            nscene.addRenderer(new ScreenSpaceRenderer(0, 100));

            nscene.addEntityProcessor(new RigidBodySystem());
            nscene.addEntityProcessor(new EnemySpawnSystem());
            nscene.addEntity(new Player());
            nscene.addEntity(new Enemy()).setPosition(400, 400);
            nscene.addEntity(new HUD());

            var map = content.Load<TiledMap>("map1");
            foreach (var coll in map.getObjectGroup("collisions").objects)
            {
                nscene.createEntity("wall")
                    .addComponent(new BoxCollider(
                        new Rectangle(coll.x, coll.y, coll.width, coll.height)
                        ) { physicsLayer = 1 << 0 })
                    ;
            }
            nscene.createEntity("map")
                .addComponent(
                new TiledMapComponent(map)
                {
                    renderLayer = 5
                })
                .entity.setScale(5);
            nscene.createEntity("shake")
                .addComponent(new CameraShake());

            return nscene;
        }

        protected override void Initialize()
        {
            base.Initialize();
            Scene.setDefaultDesignResolution(640, 360, Scene.SceneResolutionPolicy.FixedHeight);
            Window.AllowUserResizing = true;
            scene = startMenuScene();
        }

        protected override void Update(GameTime gameTime)
        {
            AttackManager.Instance.update();
            base.Update(gameTime);
        }
    }
}
