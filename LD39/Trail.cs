﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

using Nez;

namespace LD39
{
    /// <summary>
    /// Renders a trail behind a moving object
    /// </summary>
    public class Trail : RenderableComponent, IUpdatable
    {
        public override RectangleF bounds
        {
            get { return _bounds; }
        }

        /// <summary>
        /// starting color of the ribbon
        /// </summary>
        public Color startColor
        {
            get { return color; }
            set { color = value; }
        }

        /// <summary>
        /// end (tail) color of the ribbon
        /// </summary>
        public Color endColor;

        /// <summary>
        /// max pixel radius of the ribbon
        /// </summary>
        public float beginRadius = 5;
        public float endRadius = 0;

        public float trailAge = 0.16f;


        LinkedList<Vector2> _points = new LinkedList<Vector2>();
        LinkedList<float> _times = new LinkedList<float>();


        public Trail()
        { }


        #region Component/RenderableComponent/IUpdatable

        public override void onEnabled()
        {
            base.onEnabled();
        }


        public override void onAddedToEntity()
        {
            _points = new LinkedList<Vector2>();
            _times = new LinkedList<float>();

            for (int i = 0; i < 2; ++i)
            {
                _points.AddFirst(entity.position);
                _times.AddFirst(Time.time);
            }
        }


        public override void onRemovedFromEntity()
        {
        }

        void IUpdatable.update()
        {
            while (_times.Count > 0 && 
                Time.time - _times.Last.Value > trailAge)
            {
                _times.RemoveLast();
                _points.RemoveLast();
            }

            _points.AddFirst(entity.transform.position);
            _times.AddFirst(Time.time);
        }


        public override bool isVisibleFromCamera(Camera camera)
        {
            return true;
        }


        public override void render(Graphics graphics, Camera camera)
        {

            if (_points.Count < 2)
                return;
            int i = 0;
            var _ribbonLength = _points.Count;
            for (var it = _points.First.Next; it != _points.Last; it = it.Next)
            {
                var ncolor = ColorExt.lerp(color, endColor, (float) i / _ribbonLength);

                Vector2 start = it.Previous.Value, end = it.Value;
                graphics.batcher.drawLineAngle(it.Previous.Value,
                    Mathf.angleBetweenVectors(start, end), Vector2.Distance(start, end),
                    ncolor, Mathf.map(i, 0, _ribbonLength, beginRadius * entity.scale.X, endRadius * entity.scale.X)
                    );
                i++;
            }
        }

        #endregion
    }
}

