﻿
using Nez;

namespace LD39
{
    public class RigidBody : Component
    {
        Collider _collider;
        public override void onAddedToEntity()
        {
            base.onAddedToEntity();
            _collider = this.getComponent<Collider>();
        }

        public delegate void CollideDelegate(CollisionResult result);
        public event CollideDelegate Collided;

        public void Collide(CollisionResult result)
        {
            if (Collided != null)
                Collided(result);
        }
    }
}
