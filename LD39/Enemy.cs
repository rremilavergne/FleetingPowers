﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Nez;
using Nez.Sprites;
using Nez.Particles;

namespace LD39
{
    public class SpellDrop : Entity
    {
        public static int count = 0;
        public Attack atk;

        public override void onAddedToScene()
        {
            addComponent(new Trail()
            {
                trailAge = 0.167f * 5,
                color = atk.trailcolor,
                endColor = Color.Transparent,
                beginRadius = 3f
            });
        }

        public override void update()
        {
            base.update();
            var target = scene.findEntity("player").position;
            var dir = target - position;
            var velo = dir.normalized() * 700f * Time.deltaTime;
            position += velo;
            if (dir.LengthSquared() < 700 * Time.deltaTime * 700 * Time.deltaTime)
            {
                AttackManager.Instance.queue.AddLast(atk);
                destroy();
                count -= 1;
            }
                

            
        }
    }

    public class Enemy : Entity
    {
        public Entity target;
        Attack nextAtk = null;

        public override void onAddedToScene()
        {
            target = scene.findEntity("player");
            addComponent(new Sprite(
                scene.content.Load<Texture2D>("enemy")
                )).layerDepth = Mathf.map01(-position.Y, -10000, 10000);

            addComponent(new CircleCollider(10f) { physicsLayer = 1 << 2 } );
            addComponent(new RigidBody()).Collided += OnCollide;
            var config = new ParticleEmitterConfig();
            config.subtexture = new Nez.Textures.Subtexture(Core.content.Load<Texture2D>("particlewind"));
            config.emissionRate = 80.0f;
            config.sourcePositionVariance = new Vector2(20f, 20f);
            config.particleLifespan = 0.6f;
            config.duration = 0.1f;
            config.angleVariance = 360;
            config.speed = 30;
            config.rotationStartVariance = 80f;
            config.startParticleSize = 8f;
            config.finishParticleSize = 8f;
            config.startColor = Color.MediumPurple;
            config.finishColor = Color.Transparent;
            config.maxParticles = 10000;
            if (Nez.Random.nextFloat() < 1f / (AttackManager.Instance.queue.Count + SpellDrop.count) / 2)
            {
                SpellDrop.count += 1;
                nextAtk = AttackManager.GetRandomAttack().clone() as Attack;
                var nentity = scene.createEntity("");
                var nsprite = nentity.addComponent(new Sprite(
                    scene.content.Load<Texture2D>("aura")
                    )).setColor(nextAtk.trailcolor).setLayerDepth(0.5f);
                nentity.setParent(this);
                config.startColor = nextAtk.trailcolor;

            }
            addComponent(new ParticleEmitter(config))
                .play();

        }

        public virtual void OnCollide(CollisionResult result)
        {
            if (result.collider.entity.name == "wall")
                Damage();
        }

        public virtual void Damage()    
        {
            Game1.score += 1;
            if (nextAtk != null)
            {
                scene.addEntity(new SpellDrop() { atk = nextAtk }.setPosition(position));
            }
            var config = new ParticleEmitterConfig();
            config.subtexture = new Nez.Textures.Subtexture(Core.content.Load<Texture2D>("particlewind"));
            config.emissionRate = 40.0f;
            config.duration = 0.035f;
            config.particleLifespan = 0.5f;
            config.particleLifespanVariance = 0.2f;
            config.speed = 100f;
            config.speedVariance = 70f;
            config.angleVariance = 360f;
            config.startParticleSize = 16f;
            config.finishParticleSize = 8f;
            config.startColor = Color.Red;
            config.finishColor = Color.Transparent;
            config.blendFuncSource = Blend.One;
            config.blendFuncDestination = Blend.InverseSourceAlpha;
            config.maxParticles = 20000;

            var emitter = scene.createEntity("")
                .addComponent(new ParticleEmitter(config))
                .entity.setPosition(position)
                ;

            emitter.addComponent(new Timeout { timeout = config.duration + config.particleLifespan + config.particleLifespanVariance });

            destroy();
        }

        public override void update()
        {
            base.update();

            var mouse = target.position;
            var angle = position.angleBetween(position + new Vector2(1f, 0f), mouse);
            if (position.Y > mouse.Y)
                this.setRotationDegrees(-angle);
            else
                this.setRotationDegrees(angle);

            var velocity = target.position - position;
            velocity = velocity.normalized() * 50f;
            position += velocity * Time.deltaTime;
        }

    }

    public class EnemySpawnSystem : ProcessingSystem
    {
        float spawnTime = 0.5f;
        float spawnRate = 0.5f;

        public override void process()
        {
            spawnTime -= Time.deltaTime;
            while (spawnTime < 0)
            {
                Vector2 spawnPosition;
                do
                {
                    spawnPosition = new Vector2(
                    Mathf.map(Nez.Random.nextFloat(), 0f, 1f, 50f, 750f),
                    Mathf.map(Nez.Random.nextFloat(), 0f, 1f, 50f, 750f)
                    );
                } while ((spawnPosition - scene.findEntity("player").position).Length() < 200);
                var en = new Enemy();
                en.position = spawnPosition;
                scene.addEntity(en);
                spawnTime += spawnRate;

            }
        }
    }
}
